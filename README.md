# GitLab environment variables

Shows all [Gitlab CI/CD predefined enviroment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) which are available in Job on CI Runner.

If you have configured `dind` runner, you can see also all variables available during docker container build.

## Usage

It is simple, run CI/CD pipeline and check the Job result, eg. [this job](https://gitlab.com/hmlkao/gitlab-env/-/jobs/238503629)

As you can see, there is also visible variables defined in CI/CD Settings > Variables of this project, like
```
SECRET_VAR=Very secret data!!!
```

## Job facts

### env
Nativly is job running inside ruby image
```
Pulling docker image ruby:2.5
```

### env in image
Environment variables only

### env in docker
Variables available during Docker build
